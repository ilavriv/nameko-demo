from nameko.runners import ServiceRunner

from services.service_a import ServiceA
from services.service_b import ServiceB
from services.service_http import ServiceHTTP


runner = ServiceRunner(config={
  'AMQP_URI': 'pyamqp://guest:guest@localhost'
})

runner.add_service(ServiceA)
runner.add_service(ServiceB)
runner.add_service(ServiceHTTP)

if __name__ == '__main__':
    runner.start()