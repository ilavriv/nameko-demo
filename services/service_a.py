from nameko.events import EventDispatcher
from nameko.rpc import rpc

class ServiceA:
    name = "service_a"
    dispatch = EventDispatcher()

    @rpc
    def hello(self, payload):
        return "Hello"
