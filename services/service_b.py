from nameko.events import event_handler


class ServiceB:
    name = "service_b"

    @event_handler("service_http", "my_event")
    def handle_event(self, payload):
        print("Hello, {}".format(payload))