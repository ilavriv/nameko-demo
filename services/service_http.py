import json
from nameko.events import EventDispatcher
from nameko.web.handlers import http


class ServiceHTTP:
    name = "service_http"
    dispatch = EventDispatcher()

    @http('GET', '/hello/<int:value>')
    def get(self, request, value=0):
        self.dispatch('my_event', value)
        return json.dumps({ 'value': value })